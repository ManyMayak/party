package com.example.partytime

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.partytime.adapters.GuestAdapter
import com.example.partytime.pojo.Invite
import com.google.gson.Gson
import com.squareup.picasso.Picasso

class MainActivity : AppCompatActivity() {

    lateinit var imageViewParty : ImageView
    lateinit var imageViewPhotoInviter : ImageView
    lateinit var textViewTitleParty : TextView
    lateinit var textViewInvite : TextView
    lateinit var recyclerViewGuest: RecyclerView
    lateinit var guestAdapter : GuestAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val toolbar = findViewById(R.id.toolbarParty) as Toolbar
        imageViewParty = findViewById(R.id.imageViewPartyImage)
        imageViewPhotoInviter = findViewById(R.id.imageViewInviter)
        textViewTitleParty = findViewById(R.id.textViewTitleParty)
        textViewInvite = findViewById(R.id.textViewInvite)
        recyclerViewGuest = findViewById(R.id.recyclerViewGuests)
        setSupportActionBar(toolbar)

        guestAdapter = GuestAdapter()

        recyclerViewGuest.layoutManager = LinearLayoutManager(this)
        recyclerViewGuest.adapter = guestAdapter


        jsonReader()
    }

    private fun jsonReader(){
        var myJSON = resources.openRawResource(R.raw.company).
        bufferedReader().use { it.readText() }

        var invite = Gson().fromJson(myJSON, Invite::class.java)

        fillContent(invite)
    }

    private fun fillContent(invite: Invite){
        Picasso.get().isLoggingEnabled = true

        Picasso.get().load(invite.getImageParty()).into(imageViewParty)
        Picasso.get().load(invite.getPhotoInviter()).into(imageViewPhotoInviter)
        textViewTitleParty.text = invite.getTitleParty()
        textViewInvite.text = invite.getNameInviter()
        guestAdapter.update(invite.getGuest()!!)
    }

}