package com.example.partytime.adapters

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.partytime.R
import com.example.partytime.pojo.Guest
import com.squareup.picasso.Picasso

class GuestAdapter : RecyclerView.Adapter<GuestAdapter.GuestViewHolder>() {

    private var listGuest = mutableListOf<Guest>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GuestViewHolder {
        var view = LayoutInflater.from(parent.context).
            inflate(R.layout.item_guest, parent, false)
        return GuestViewHolder(view)
    }

    override fun onBindViewHolder(holder: GuestViewHolder, position: Int) {
        var guest = listGuest[position]
        Picasso.get().load(guest.getPhotoGuest()).into(holder.imageViewGuestPhoto)
        holder.textViewGuestName.text = guest.getNameGuest()
    }

    override fun getItemCount(): Int {
        return listGuest.size
    }

    fun update(listGuest: MutableList<Guest>){
        this.listGuest = listGuest
        notifyDataSetChanged()
    }

    class GuestViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var imageViewGuestPhoto: ImageView = itemView.findViewById(R.id.imageViewGuestPhoto)
        var textViewGuestName: TextView = itemView.findViewById(R.id.textViewGuest)

    }

}