package com.example.partytime.pojo

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Guest {

    @SerializedName("name_guest")
    @Expose
    private var nameGuest: String? = null

    @SerializedName("photo_guest")
    @Expose
    private var photoGuest: String? = null

    fun getNameGuest(): String? {
        return nameGuest
    }

    fun setNameGuest(nameGuest: String?) {
        this.nameGuest = nameGuest
    }

    fun getPhotoGuest(): String? {
        return photoGuest
    }

    fun setPhotoGuest(photoGuest: String?) {
        this.photoGuest = photoGuest
    }

}