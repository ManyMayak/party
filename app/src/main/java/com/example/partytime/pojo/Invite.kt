package com.example.partytime.pojo

import com.google.gson.annotations.Expose

import com.google.gson.annotations.SerializedName




class Invite {

    @SerializedName("image_party")
    @Expose
    private var imageParty: String? = null

    @SerializedName("title_party")
    @Expose
    private var titleParty: String? = null

    @SerializedName("name_inviter")
    @Expose
    private var nameInviter: String? = null

    @SerializedName("photo_inviter")
    @Expose
    private var photoInviter: String? = null

    @SerializedName("guest")
    @Expose
    private var guest: MutableList<Guest>? = null

    fun getImageParty(): String? {
        return imageParty
    }

    fun setImageParty(imageParty: String?) {
        this.imageParty = imageParty
    }

    fun getTitleParty(): String? {
        return titleParty
    }

    fun setTitleParty(titleParty: String?) {
        this.titleParty = titleParty
    }

    fun getNameInviter(): String? {
        return nameInviter
    }

    fun setNameInviter(nameInviter: String?) {
        this.nameInviter = nameInviter
    }

    fun getPhotoInviter(): String? {
        return photoInviter
    }

    fun setPhotoInviter(photoInviter: String?) {
        this.photoInviter = photoInviter
    }

    fun getGuest(): MutableList<Guest>? {
        return guest
    }

    fun setGuest(guest: MutableList<Guest>) {
        this.guest = guest
    }

}